import React from "react";
import {BrowserRouter, Route, Switch, NavLink} from "react-router-dom";
import Home from "./Components/Home/Home";
import StageTwo from "./Components/StageTwo/StageTwo";
import "./App.css";

const App = () => {
  
  return (
    <div className="App">
      <BrowserRouter>
        <div className="navBarDiv">
          <nav className="navBar">
            <NavLink to=''>Home</NavLink>
            <NavLink to={'/StageTwo'}>Stage 2</NavLink>
          </nav>
        </div>

        <Switch>
          <Route exact path="/">
            <Home />
          </Route>
          <Route path="/StageTwo">
            <StageTwo />
          </Route>
        </Switch>
      </BrowserRouter>
    </div>
  );
};
export default App;
