import { useState } from "react";
import Axios from "axios";
import { FaStar, FaSadTear } from "react-icons/fa";
import "../StageTwo/StageTwo.css";

export default function StageTwo() {

    const [pokeList, setPokeList] = useState([]);

    const listPokemonStage2 = (props) => {
        Axios.get(`https://pokeapi.co/api/v2/pokemon`).then(
        (res) => {
                console.log(res.data.results);
                setPokeList(
                    res.data.results
                );
            }
        );
    };

    return (
        <div>
            <div className="App">
                <div className="stageTitleSection">
                    <h1>Pokémons Names</h1>
                     <button onClick={listPokemonStage2}>
                        Click me to see magic <FaStar className="starIcon"/>
                    </button>
                </div>
                <div className="showNames">
                    { pokeList.length > 0 ?
                        pokeList.map((elem) => (
                            <p className="namePokeText">{elem.name}</p>
                        ))
                        :
                        <p>No Pokemon today <FaSadTear className="sadIcon"/></p>
                    }
                </div>
            </div>
        </div>
    )
}